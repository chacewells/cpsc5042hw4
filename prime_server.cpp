#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>         // close
#include <fcntl.h>          // fcntl, F_SETFL
#include <pthread.h>        // pthread_mutex_t, pthread_*
#include <queue>            // std::queue
#include <vector>           // std::vector
#include <iostream>
#include <fstream>          // ofstream
#include <math.h>           // sqrt
#include <time.h>           // time

#define PORT 7777
#define PRIMES_FILE "P.txt"
#define READY_QUEUE_LIMIT 100000
#define MAX_WAIT 5

// also included in client
struct PrimeCheck {
    long number;
    bool isPrime = true;
    PrimeCheck(long n) {
        number = n;
    };
    PrimeCheck(const PrimeCheck &o): number(o.number), isPrime(o.isPrime) {};
    PrimeCheck() {}
};

struct Client {
    int sock;
    time_t *timeSent = nullptr;
    PrimeCheck *dataWaitedOn = nullptr;
    Client(int s): sock(s) {};
};

// server struct
struct Server {
    int sock;
    struct sockaddr_in address;
    unsigned int addressLength;
    Server() {
        sock = socket(AF_INET, SOCK_STREAM, 0);
        address.sin_family = AF_INET;
        address.sin_addr.s_addr = INADDR_ANY;
        address.sin_port = htons(PORT);
        addressLength = sizeof(address);
    };
};

Server server;

// ready queue for prime analysis
std::queue<PrimeCheck> readyQueue;
// lock ready queue
pthread_mutex_t readyQueueLock;
// client connections
std::vector<Client> clientPool;
// lock for clientPool
pthread_mutex_t clientPoolLock;

// thread runners
pthread_t numberSequencerThread;
pthread_t clientListenerThread;

// primes file
std::ofstream primesFile;

/* Calculates whether the number attribute is prime
 * If it is not, the isPrime flag is set to false
 */
void checkIsPrime(PrimeCheck&);

/*
 * Generates a sequence of PrimeCheck corresponding to 2..Inf
 * and adds each to the ready queue
 */
void *numberSequencer(void *);

/*
 * Listens for client connections,
 * then adds them to the client pool
 */
void *clientListener(void *);

int main() {
    primesFile.open(PRIMES_FILE);
    bind(server.sock, (struct sockaddr *)&server.address, server.addressLength);

    pthread_mutex_init(&readyQueueLock, nullptr);
    pthread_mutex_init(&clientPoolLock, nullptr);

    pthread_create(&numberSequencerThread, nullptr, numberSequencer, nullptr);
    pthread_create(&clientListenerThread, nullptr, clientListener, nullptr);
    
    while (true) { // main loop; send/recv with clients
        pthread_mutex_lock(&clientPoolLock);
        for (auto client = clientPool.begin(); client < clientPool.end();) {
            pthread_mutex_lock(&readyQueueLock);
            if (client->timeSent == nullptr && !readyQueue.empty()) {
                PrimeCheck message = readyQueue.front();
                readyQueue.pop();
                pthread_mutex_unlock(&readyQueueLock);
                if (write(client->sock, &message, sizeof(PrimeCheck)) != -1) {
                    // set time sent
                    client->timeSent = new time_t;
                    time(client->timeSent);
                    client->dataWaitedOn = new PrimeCheck(message.number);;
                    ++client;
                } else { // bad send; reap client
                    std::cout << "Error while sending to client " << client->sock << "; Closing";
                    close(client->sock);
                    clientPool.erase(client);
                    // place data back on ready queue
                    pthread_mutex_lock(&readyQueueLock);
                    readyQueue.push(message);
                    pthread_mutex_unlock(&readyQueueLock);
                }
            } else {
                pthread_mutex_unlock(&readyQueueLock);
                ++client;
            }
        }
        pthread_mutex_unlock(&clientPoolLock);

        pthread_mutex_lock(&readyQueueLock);
        if (!readyQueue.empty()) {
            PrimeCheck localCheck = readyQueue.front();
            readyQueue.pop();
            checkIsPrime(localCheck);
            if (localCheck.isPrime)
                primesFile << localCheck.number << std::endl;
        }
        pthread_mutex_unlock(&readyQueueLock);

        pthread_mutex_lock(&clientPoolLock);
        for (auto client = clientPool.begin(); client < clientPool.end();) {
            int origFlags = fcntl(client->sock, F_GETFL, nullptr);
            fcntl(client->sock, F_SETFL, origFlags|O_NONBLOCK);
            PrimeCheck primeCheck;
            int bytesRead = read(client->sock, &primeCheck, sizeof(PrimeCheck));
            fcntl(client->sock, F_SETFL, origFlags);
            if (bytesRead > 0) {
                // got a response; add to results if prime
                if (primeCheck.isPrime)
                    primesFile << primeCheck.number << std::endl;
                delete client->timeSent;
                client->timeSent = nullptr;
                ++client;
            } else {
                time_t now;
                time(&now);
                if ( client->timeSent != nullptr && (now - *(client->timeSent)) >= MAX_WAIT ) { // long wait, so reap
                    std::cout << "Waited too long; closing " << client->sock << std::endl;
                    close(client->sock);
                    delete client->timeSent;
                    client->timeSent = nullptr;
                    if (client->dataWaitedOn != nullptr) {
                        PrimeCheck unused(client->dataWaitedOn->number);
                        pthread_mutex_lock(&readyQueueLock);
                        readyQueue.push(unused);
                        pthread_mutex_unlock(&readyQueueLock);
                        delete client->dataWaitedOn;
                        client->dataWaitedOn = nullptr;
                    }
                    clientPool.erase(client);
                } else {
                    ++client;
                }
            }
        }
        pthread_mutex_unlock(&clientPoolLock);
    }

    pthread_join(numberSequencerThread, nullptr);
    pthread_join(clientListenerThread, nullptr);

    return 0;
}

void checkIsPrime(PrimeCheck &primeCheck) {
    long squareRoot = (long)floor(sqrt((double)primeCheck.number));
    for (int i = 2; i <= squareRoot; ++i)
        if (primeCheck.number % i == 0) {
            primeCheck.isPrime = false;
            break;
        }
}

void *numberSequencer(void *) {
    for (long curr = 2;; ++curr) { // loop forever for now
        pthread_mutex_lock(&readyQueueLock);
        if (readyQueue.size() > READY_QUEUE_LIMIT) {
            pthread_mutex_unlock(&readyQueueLock);
            sleep(1);
        } else {
            PrimeCheck primeCheck(curr);
            readyQueue.push(primeCheck);
            pthread_mutex_unlock(&readyQueueLock);
        }
    }

    pthread_exit(0);
    return nullptr;
}

void *clientListener(void *) {
    while (listen(server.sock, 0) == 0) {
        int sock = accept(server.sock, (struct sockaddr *)&server.address, &server.addressLength);
        std::cout << "Connected to new client: " << sock << std::endl;
        Client client(sock);
        pthread_mutex_lock(&clientPoolLock);
        clientPool.push_back(client);
        pthread_mutex_unlock(&clientPoolLock);
    }

    pthread_exit(0);
    return nullptr;
}

