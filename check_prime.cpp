#include <iostream>
#include <math.h>

using namespace std;

struct PrimeCheck {
    long number;
    bool isPrime = true;
    PrimeCheck(long n) {
        number = n;
    };
    PrimeCheck() {}
};

void checkIsPrime(PrimeCheck &primeCheck) {
    long squareRoot = (long)floor(sqrt((double)primeCheck.number));
    for (int i = 2; i <= squareRoot; ++i)
        if (primeCheck.number % i == 0) {
            cout << primeCheck.number << " is divisible by " << i << endl;
            primeCheck.isPrime = false;
            break;
        }
}

int main() {
    PrimeCheck pc(239893);
    checkIsPrime(pc);
    cout << pc.number << " is " << (pc.isPrime ? "" : "not ") << "prime" << endl;
    return 0;
}
