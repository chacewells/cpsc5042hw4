use strict;
use warnings 'all';
use Socket;

use constant {
    PrimeCheck => 'q C x![q]',
    HOST => 'localhost',
    PORT => 7777,
    PrimeCheck_SIZE => 16,
};

my $iaddr = inet_aton "localhost";
my $proto = getprotobyname "tcp";
my $paddr = sockaddr_in(PORT, $iaddr);
socket(PRIME_SERVER, PF_INET, SOCK_STREAM, $proto);
connect(PRIME_SERVER, $paddr) or die "connect: $!";

$| = 1;
my $data;
while (read(PRIME_SERVER, $data, PrimeCheck_SIZE)) {
    my ($number, $is_prime) = unpack PrimeCheck, $data;
    # print "number: $number; is_prime: $is_prime\n";
    print "Calculating prime for $number...\n";
    $is_prime = checkIsPrime($number);
    print "$number is ", $is_prime ? '' : 'not ', "prime\n";
    my $ret = pack PrimeCheck, $number, $is_prime;
    print PRIME_SERVER $ret;
}

sub checkIsPrime {
    my $number = shift;
    for (my $i = 2; $i*$i <= $number; ++$i) {
        return 0 if $number % $i == 0;
    }
    return 1;
}

