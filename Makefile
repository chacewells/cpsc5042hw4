CC = g++
CPPFLAGS = -std=c++11 -Wall -lpthread
CLIENT_PROGRAM = prime_client
SERVER_PROGRAM = prime_server

all : $(CLIENT_PROGRAM) $(SERVER_PROGRAM)

server : $(SERVER_PROGRAM).cpp
	$(CC) $(CPPFLAGS) $(SERVER_PROGRAM).cpp -o $(SERVER_PROGRAM)

client : $(CLIENT_PROGRAM).cpp
	$(CC) $(CPPFLAGS) $(CLIENT_PROGRAM).cpp -o $(CLIENT_PROGRAM)

clean :
	if [[ -f $(CLIENT_PROGRAM) ]]; then rm $(CLIENT_PROGRAM); fi
	if [[ -f $(SERVER_PROGRAM) ]]; then rm $(SERVER_PROGRAM); fi
