use constant PrimeCheck => "q C x![q]";

($number, $isPrime) = (7, 1);

$struct = pack PrimeCheck, $number, $isPrime;
open OUT, '>:raw', 'prime_check.bin';
print OUT $struct;
close $out;

