#include <cstdio>

struct PrimeCheck {
    long number;
    bool isPrime;
};

int main() {
    FILE *fin;
    struct PrimeCheck *primeCheck;

    printf("sizeof(PrimeCheck): %d\n", sizeof(PrimeCheck));
    fin = fopen("prime_check.bin", "rb");
    fread(primeCheck, sizeof(PrimeCheck), 1, fin);
    fclose(fin);

    printf("number: %ld\n", primeCheck->number);
    printf("isPrime: %d\n", primeCheck->isPrime);
    return 0;
}

