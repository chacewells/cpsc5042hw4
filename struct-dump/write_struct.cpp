#include <cstdio>

struct PrimeCheck {
    long number;
    bool isPrime;
};

int main() {
    FILE *fout;
    struct PrimeCheck *primeCheck = new PrimeCheck { 15, true };
    fout = fopen("prime_check.bin", "wb");
    fwrite(primeCheck, sizeof(PrimeCheck), 1, fout);
    fclose(fout);
    return 0;
}
