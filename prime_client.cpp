#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>         // close
#include <math.h>           // sqrt
#include <iostream>

#define HOST "127.0.0.1"
#define PORT 7777

// also included in server
struct PrimeCheck {
    long number;
    bool isPrime;
};

/*
 * Calculates whether the number attribute is prime
 * If it is not, the isPrime flag is set to false
 */
void checkIsPrime(PrimeCheck&);

int main() {
    int sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock < 0) {
        std::cerr << "Failed to make socket" << std::endl;
        return 1;
    }

    struct sockaddr_in address;
    address.sin_family = AF_INET;
    address.sin_port = htons(PORT);
    if (inet_pton(AF_INET, HOST, &address.sin_addr) <= 0) {
        std::cerr << "Invalid address or address not supported" << std::endl;
        return 1;
    }
    socklen_t addressLength = sizeof(sockaddr_in);

    if ( connect(sock, (struct sockaddr *)&address, addressLength) == -1 ) {
        std::cerr << "Failed to connect to " << HOST << ":" + PORT << ": " << errno << std::endl;
        return 1;
    }

    while (true) {
        PrimeCheck primeCheck;
        if ( read(sock, &primeCheck, sizeof(PrimeCheck)) == -1 ) {
            std::cerr << "Failed to read from server: " << errno << std::endl;
            return 1;
        }

        std::cout << "Calculating prime for " << primeCheck.number << std::endl;
        checkIsPrime(primeCheck);
        std::cout << primeCheck.number << " is " << (primeCheck.isPrime ? "" : "not ") << "prime." << std::endl;

        write(sock, &primeCheck, sizeof(PrimeCheck));
    }

    return 0;
}

void checkIsPrime(PrimeCheck &primeCheck) {
    long squareRoot = (long)floor(sqrt((double)primeCheck.number));
    for (int i = 2; i < squareRoot; ++i)
        if (primeCheck.number % i == 0) {
            primeCheck.isPrime = false;
            break;
        }
}
