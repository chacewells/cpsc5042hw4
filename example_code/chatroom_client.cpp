//==============================================================================
// AUTHOR: Scott Moser
// FILENAME: chatroom_server.cpp
// DATE: 06/12/2018
// REVISION HISTORY: 1.0.0
// REFERENCES: 
//
// Build Command: g++ -pthread chatroom_client.cpp -o client
//
// Usage: ./client <name>
//
// Pledge:
// I have not received unauthorized aid on this assignment. I understand the
// answers that I have submitted. The answers submitted have not been directly
// copied from another source, but instead are written in my own words.
//
// Problem Statement:
// In this assignment your job is to build a chat room. For this purpose, you
// should write a server and a client using sockets in C/C++. The goal is that
// if N clients are connected to the server, any message that is sent by any
// client is broadcasted to all the other clients. 
//
// Design Notes:
//  - Implemented a reader thread to handle client input so the main loop
//    can focus on interacting with the server
//  - Using a mutex to protect a data structure shared between main() and the
//    reader thread
//  - Implemented a signal handler to help gracefully shutdown the client in
//    case the user panics and types CTRL-C
//  - Implemented fixed message lengths for simplicity
//
// Assumptions:
//  - The client does not need to key off of the "\token" string. Instead, I'm
//    using a message ID enumeration to handle message types.
//  - CPU usage is not a concern for this assignment.
//==============================================================================

//==============================================================================
// OS-defined header files
//==============================================================================
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <signal.h>
#include <pthread.h>

//==============================================================================
// Local static constants
//==============================================================================
// Developer Note: Since we don't have a shared header file, keep in-sync
// with chatroom_server.cpp.
static const int PORT = 8082;

// Client commands
static const char * EXIT_CMD = "\\end\n";  // New-line needed for strcmp() match
static const char * PASS_CMD = "\\pass";
static const char * HELLO_STR = "Hello, World!";
static const char * PROMPT_STR = ">> ";

//==============================================================================
// Type definitions
//==============================================================================

// Developer Note: Since we don't have a shared header file, keep in-sync
// with chatroom_server.cpp.
// sizeof(Message_ID_e) should equal 4 bytes
typedef enum
{
    Message_ID_Pass = 0,   // Pass - give up a turn
    Message_ID_Message,    // Send a message
    Message_ID_Hello,      // Introduce self to chatroom
    Message_ID_Goodbye,    // Leave the chatroom
    Message_ID_Token,      // *** SERVER ONLY ***
    Message_ID_Invalid = 0xFFFFFFFF  // Force to 32-bit alignment
} Message_ID_e;

// Developer Note: Since we don't have a shared header file, keep in-sync
// with chatroom_server.cpp. Keep fixed size for convenience.
// Fixed packet length for socket messages to make life easier
// sizeof(packet_s) should equal 2048 bytes (2KB)
typedef struct
{
    Message_ID_e ID;
    char username[16];
    char payload[2028];  // 2048 - 16 - 4
} packet_s;

//==============================================================================
// Local function prototypes
//==============================================================================
static void intHandler(int sig);
static void *reader(void * param);

//==============================================================================
// Global variables
//==============================================================================
volatile bool runFlag_g = true;
pthread_mutex_t lock_g;

//==============================================================================
// Function definitions
//==============================================================================

//==============================================================================
// Description: Main function.
// Input:  argc - Number of command-line arguments (always >= 1)
// Input:  argv[] - Array of strings
// Modify: None
// Output: EXIT_SUCCESS if successful, EXIT_FAILURE if not successful
//==============================================================================
int main(int argc, char const *argv[]) 
{
    // Initialize local variables
    int sock = 0;
    int valread;
    struct sockaddr_in serv_addr;

    // Create incoming and outgoing packets
    packet_s pkt_in;
    packet_s pkt_out;

    // Register interrupt handler
    signal(SIGINT, intHandler);

    // Rough input validation
    if (argc != 2)
    {
        printf("\nUsage:\n");
        printf("./client <name>:\n");
        return EXIT_SUCCESS;
    }

    // Initialize mutex
    if(pthread_mutex_init(&lock_g, NULL) != 0)
    {
        printf("ERROR: Unable to initialize mutex!\n");
        return EXIT_FAILURE;
    }

    // Create test packet. This is the first packet sent to the server
    // to announce presence in the chatroom.
    packet_s pkt_hello;
    pkt_hello.ID = Message_ID_Hello;
    strncpy(pkt_hello.username, argv[1], sizeof(pkt_hello.username));
    strncpy(pkt_hello.payload, HELLO_STR, sizeof(pkt_hello.payload));
    
    // Initialize send packet to \pass
    pkt_out.ID = Message_ID_Pass;
    strncpy(pkt_out.username, argv[1], sizeof(pkt_out.username));
    strncpy(pkt_out.payload, PASS_CMD, sizeof(pkt_out.payload));

    // Get client socket file descriptor
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) 
    {
        printf("ERROR: Socket creation error!\n");
        return EXIT_FAILURE;
    }

    // Create a listening thread to listen for clients
    pthread_t tid;
    pthread_attr_t attr;
    pthread_attr_init(&attr);  // Set to default attributes
    pthread_create(&tid, &attr, reader, &pkt_out);

    // Zero out struct to set to default values
    memset(&serv_addr, '0', sizeof(serv_addr));

    // Set to IPv4 and desired port #
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    // Convert IPv4 and IPv6 addresses from text to binary form
    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0) 
    {
        printf("ERROR: Invalid address or address not supported!\n");
        return EXIT_FAILURE;
    }

    // Connect to the server
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
        printf("ERROR: Connection failed!\n");
        return EXIT_FAILURE;
    }

    // Say hello to the server
    send(sock, (void*)&pkt_hello, sizeof(packet_s), 0);

    // Main client processing loop
    while(runFlag_g)
    {
        // Zero out input packet buffer so we always have a clean slate
        memset(&pkt_in, 0, sizeof(packet_s));

        // Listen for incoming messages
        valread = read(sock, (void*)&pkt_in, sizeof(packet_s));

        // Verify we read a full packet
        if (valread == sizeof(packet_s))
        {           
            // Switch based on message ID
            switch (pkt_in.ID)
            {
                case Message_ID_Hello:
                {
                    // Announce entrance to client console
                    printf("%s joined\n", pkt_in.username);
                } break;

                case Message_ID_Goodbye:
                {
                    // Announce exit to client console
                    printf("%s left\n", pkt_in.username);
                } break;

                case Message_ID_Message:
                {
                    // Print message to console
                    printf("%s: %s", pkt_in.username, pkt_in.payload);
                } break;

                case Message_ID_Token:
                {
                    // Send a message to the server
                    pthread_mutex_lock(&lock_g);
                    send(sock, (void*)&pkt_out, sizeof(packet_s), 0);

                    // Toggle back to \pass after sending a message
                    pkt_out.ID = Message_ID_Pass;
                    strncpy(pkt_out.payload, PASS_CMD, sizeof(pkt_out.payload));
                    pthread_mutex_unlock(&lock_g);
                } break;

                default:
                {
                    printf("ERROR: Unknown message type!\n");
                } break;
            }
        } else
        {
            perror("Server read");
        }
    }

    // Send a goodbye message, then shutdown
    pthread_mutex_lock(&lock_g);
    pkt_out.ID = Message_ID_Goodbye;
    send(sock, (void*)&pkt_out, sizeof(packet_s), 0);
    pthread_mutex_unlock(&lock_g);

    // Notify user we are shutting down and give ample time for server
    // to see the message.
    printf("Client is shutting down...\n");
    sleep(1);

    return EXIT_SUCCESS;
}


//==============================================================================
// Description: Signal handler for the client.
// Input: int sig - Signal ID
// Modify: Updates global runFlag.
// Return: None - void
//==============================================================================
void intHandler(int sig)
{
    // Capture a CTRL-C so we can terminate the program gracefully
    if (sig == SIGINT)
    {
        runFlag_g = false;        
    }
}


//==============================================================================
// Description: Thread that listens for command-line input and updates client's
//              outgoing packet buffer.
// Input:  void * param - Pointer to the client's outgoing packet buffer.
// Modify: Performs R/M/W on the outgoing packet buffer.
// Return: None - void
//==============================================================================
static void *reader(void * param)
{
    packet_s * pkt_out = (packet_s *)param;
    packet_s pkt_local;
    char * buffer = (char*)malloc(sizeof(pkt_local.payload));
    size_t nBytes = (size_t)sizeof(pkt_local.payload);

    // Parse command-line for data, then update struct
    while(runFlag_g)
    {
        // Parse command-line
        printf(PROMPT_STR);
        getline(&buffer, &nBytes, stdin);  // Blocking call

        // Check for exit command byte sequence
        if (strcmp(buffer, EXIT_CMD) == 0)
        {
            runFlag_g = false;
            while(1);  // Hang this thread until program exits
        }

        // Read-in incoming variable to local variable
        pkt_local = *pkt_out;

        // Mark as a data message
        pkt_local.ID = Message_ID_Message;

        // Decide whether to replace or concatenate messages
        if (strcmp(pkt_local.payload, PASS_CMD) == 0)
        {
            strncpy(pkt_local.payload, buffer, sizeof(pkt_local.payload));    
        } else
        {
            strncat(pkt_local.payload, buffer, sizeof(pkt_local.payload));
        }
        
        // Write to out var
        pthread_mutex_lock(&lock_g);
        memcpy(pkt_out, &pkt_local, sizeof(packet_s));
        pthread_mutex_unlock(&lock_g);
    }    

    free(buffer);
    pthread_exit(0);
}
