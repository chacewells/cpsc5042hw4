//==============================================================================
// AUTHOR: Scott Moser
// FILENAME: chatroom_server.cpp
// DATE: 06/12/2018
// REVISION HISTORY: 1.0.0
// REFERENCES: 
//
// Build Command: g++ -pthread chatroom_server.cpp -o server
//
// Usage: ./server
//
// Pledge:
// I have not received unauthorized aid on this assignment. I understand the
// answers that I have submitted. The answers submitted have not been directly
// copied from another source, but instead are written in my own words.
//
// Problem Statement:
// In this assignment your job is to build a chat room. For this purpose, you
// should write a server and a client using sockets in C/C++. The goal is that
// if N clients are connected to the server, any message that is sent by any
// client is broadcasted to all the other clients. 
//
// Design Notes:
//  - Implemented a listener thread to handle client connection so the main 
//    loop can focus on interacting with the clients
//  - Using a mutex to protect linked list integrity during the push()/pop()
//    operations.
//  - Implemented a linked list to hold active client connections
//  - Attempt to handle random client dropouts mid-transaction so the server
//    can drop the client and continue functioning with the remaining clients
//  - Implemented fixed message lengths for simplicity
//  - The server can be shut down, but is designed to be started and run
//    continuously
//
// Assumptions:
//  - The server and clients do not need to key off of escape sequence commands.
//    This implementation uses a message ID enumeration to handle message types
//    instead.
//  - CPU usage is not a concern for this assignment.
//==============================================================================

//==============================================================================
// OS-defined header files
//==============================================================================
#include <unistd.h>
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <pthread.h>

//==============================================================================
// #define macros
//==============================================================================
#undef DEBUG_ON
//#define DEBUG_ON  // Uncomment to enable debug output

//==============================================================================
// Local static constants
//==============================================================================
// Developer Note: Since we don't have a shared header file, keep in-sync
// with chatroom_client.cpp.
static const int PORT = 8082;

// Server commands
static const char * TOKEN_CMD = "\\token";

//==============================================================================
// Type definitions
//==============================================================================

// A linked list node
typedef struct node_s
{
    int fd;           // Client file descriptor
    node_s * next;    // Pointer to next node
} node_s;

// Struct for holding server information
typedef struct
{
    int server_fd;
    struct sockaddr_in address;
    int addrlen;
    node_s * client_list;
} server_s;

// Developer Note: Since we don't have a shared header file, keep in-sync
// with chatroom_client.cpp.
// Identification codes for socket messages
// sizeof(Message_ID_e) should equal 4 bytes
typedef enum
{
    Message_ID_Pass = 0,   // Pass - give up a turn
    Message_ID_Message,    // Send a message
    Message_ID_Hello,      // Introduce self to chatroom
    Message_ID_Goodbye,    // Leave the chatroom
    Message_ID_Token,      // *** SERVER ONLY ***
    Message_ID_Invalid = 0xFFFFFFFF  // Force to 32-bit alignment
} Message_ID_e;

// Developer Note: Since we don't have a shared header file, keep in-sync
// with chatroom_client.cpp.
// Fixed packet length for socket messages to make life easier
// sizeof(packet_s) should equal 2048 bytes (2KB)
typedef struct
{
    Message_ID_e ID;
    char username[16];
    char payload[2028];  // 2048 - 16 - 4
} packet_s;

//==============================================================================
// Local function prototypes
//==============================================================================
static void *listener(void * param);
static void push(int fd, node_s ** node);
static int pop(int fd, node_s ** node);
static void destroyList(node_s ** head);
static void relayPacket(node_s ** list, int fd, packet_s * tx_pkt);

#ifdef DEBUG_ON
static int getNumNodes(node_s * node);
static void printList(node_s * node);
#endif

//==============================================================================
// Global variables
//==============================================================================
pthread_mutex_t lock_g;

//==============================================================================
// Function definitions
//==============================================================================

//==============================================================================
// Description: Main function.
// Input:  argc - Number of command-line arguments (always >= 1)
// Input:  argv[] - Array of strings
// Modify: None
// Output: EXIT_SUCCESS if successful, EXIT_FAILURE if not successful
//==============================================================================
int main (int argc, char const *argv[]) 
{
	// Initialize local variables
    bool isRun = true;
    bool isWaiting = true;
    int valread = 0;
    node_s * client;
    packet_s pkt_in;

    // Create handle to server object
    server_s hdl;
    int opt = 1;

    // Zero out the structure elements
    memset(&hdl.address, 0, sizeof(sockaddr_in));

    // Initialize mutex
    if(pthread_mutex_init(&lock_g, NULL) != 0)
    {
        printf("ERROR: Unable to initialize mutex!\n");
        return EXIT_FAILURE;
    }

    // Create token packet. This is to be passed around to clients in a
    // round-robin fashion to allow them to send messages to the server.
    packet_s pkt_token;
    pkt_token.ID = Message_ID_Token;
    strncpy(pkt_token.username, "Server", sizeof(pkt_token.username));
    strncpy(pkt_token.payload, TOKEN_CMD, sizeof(pkt_token.payload));

    // Creating socket file descriptor
    if ((hdl.server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) 
    {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    // Helps in reuse of address and port. 
    // Prevents the "address already in use" error.
    if (setsockopt(hdl.server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT,
                                                  &opt, sizeof(int)))
    {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    // Used by accept()
    hdl.addrlen = sizeof(hdl.address);

    // Forcefully attaching socket to the port
    hdl.address.sin_family = AF_INET;
    hdl.address.sin_addr.s_addr = INADDR_ANY;
    hdl.address.sin_port = htons( PORT );
    if (bind(hdl.server_fd, (struct sockaddr *)&hdl.address,
                   sizeof(hdl.address)) < 0) 
    {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }
    
    // Create a listening thread to listen for clients
    pthread_t tid;
    pthread_attr_t attr;
    pthread_attr_init(&attr);  // Set to default attributes
    pthread_create(&tid, &attr, listener, &hdl);
    
    // Main server processing loop
    while (isRun)
    { 
    	// Reset client to head of linked list
        client = hdl.client_list;
        
        // Loop through all clients
        while(client != NULL)
        {
            // Zero out incoming packet to ensure we get new data
            memset(&pkt_in, 0, sizeof(packet_s));

            // Attempt to handle a dropped client mid-transaction rather than erroring out.
            // There are probably better/cleaner ways to do this.
            if (recv(client->fd, (void*)&pkt_in, sizeof(packet_s), MSG_PEEK | MSG_DONTWAIT) == 0)
            {
                printf("ERROR: Connection failure --> removing client from list!\n");
                close(client->fd);
                pop(client->fd, &hdl.client_list);
                isWaiting = true;
                break;
            }

            // Send token to the client
            send(client->fd, (void*)&pkt_token, sizeof(packet_s), 0);

            // Read and parse client's response
            valread = read(client->fd, (void*)&pkt_in, sizeof(packet_s));
            if (valread == sizeof(packet_s))
            {               
                // Switch based on message ID
                switch (pkt_in.ID)
                {
                    case Message_ID_Hello:
                    {
                        // Announce entrance to server console
                        printf("%s joined\n", pkt_in.username);
                        
                        // Send to all other clients
                        relayPacket(&hdl.client_list, client->fd, &pkt_in);

                        // Update flag for messaging
                        isWaiting = true;
                    } break;

                    case Message_ID_Goodbye:
                    {
                        // Announce exit to server console
                        printf("%s left\n", pkt_in.username);
                        
                        // Send to all other clients
                        relayPacket(&hdl.client_list, client->fd, &pkt_in);

                        // Drop client from list
                        pop(client->fd, &hdl.client_list);

                        // Update flag for messaging
                        isWaiting = true;
                    } break;

                    case Message_ID_Message:
                    {
                        // Relay to all other clients
                        relayPacket(&hdl.client_list, client->fd, &pkt_in);
                    } break;

                    case Message_ID_Pass:
                    {
                        // DO NOTHING --> proceed to next client
                    } break;

                    default:
                    {
                        printf("ERROR: Unknown message type!\n");
                    } break;
                }
            } else
            {
                perror("Client read");
            }

            // Move on to next client
            client = client->next;

            #ifdef DEBUG_ON
            printf("# nodes = %i\n", getNumNodes(hdl.client_list));
            printList(hdl.client_list);
            #endif
        }

        // Only print if this is a new state
        if (isWaiting)
        {
            printf("[Waiting ...]\n");
            isWaiting = false;
        }
    }
    
    // Should never get here with this design!
    printf("Shutting down server...\n");
    
    // Free dynamically allocated memory
    destroyList(&hdl.client_list);

    return EXIT_SUCCESS;
}


//==============================================================================
// Description: Listener thread for the server. Accepts clients and adds them
//              to the linked list of clients.
// Input:  void * param - Handle to the server's information.
// Modify: Updates linked list of clients.
// Return: None - void
//==============================================================================
static void *listener(void * param)
{
    server_s * hdl = (server_s *)param;
    bool isRun = 1;
    int tmp_fd = 0;

    // Check for valid handle
    if (!hdl)
    {
        printf("ERROR: Null handle!\n");
    }

    while(isRun)
    {
        // Listen for incoming connections
        if (listen(hdl->server_fd, 3) < 0) 
        {
            perror("listen");
            exit(EXIT_FAILURE);
        }

        // Accept an incoming connection
        tmp_fd = accept(hdl->server_fd, (struct sockaddr *)&(hdl->address),
                                                   (socklen_t*)&(hdl->addrlen));

        // Check if successful
        if (tmp_fd < 0)
        {
            perror("accept");
            exit(EXIT_FAILURE);
        } else
        {
            // TODO: Add mutex around this action?
            // Add to linked list        
            push(tmp_fd, &hdl->client_list);
        }
    }

    pthread_exit(0);
}


//==============================================================================
// Description: Sends a packet to all clients in the linked list except for the
//              client from which the message originated.
// Input:  node_s ** list - Double pointer to the head of the list.
// Input:  int fd - File descriptor of sender
// Input:  packet_s * pkt - Pointer to packet to send
// Modify: None
// Return: None - void
//==============================================================================
static void relayPacket(node_s ** list, int fd, packet_s * pkt)
{
    node_s * client = *list;

    // Loop through all clients
    while(client != NULL)
    {
        if (client->fd != fd)
        {
           send(client->fd, pkt, sizeof(packet_s), 0);
        }
        client = client->next;
    }
}


//==============================================================================
// Description: Adds a client to the beggining of the linked list.
// Input:  node_s ** head - Double pointer to head of list.
// Modify: The head reference.
// Return: None - void
//==============================================================================
static void push(int fd, node_s ** head)
{
    pthread_mutex_lock(&lock_g);
    node_s * p = (node_s*)malloc(sizeof(node_s));
    p->fd = fd;
    p->next = *head;
    *head = p;
    pthread_mutex_unlock(&lock_g);
}


//==============================================================================
// Description: Searches for and removed the specified client from the linked
//              list if found.
// Input:  node_s ** head - Double pointer to head of list.
// Modify: Removes the client identified by fd from the list.
// Return: int - 0 if success, -1 if NULL list, -2 if client not found
//==============================================================================
static int pop(int fd, node_s ** head)
{
    // Return value
    int rv = 0;  // 0 <--> success

    // Store head reference
    node_s * curr = *head;
    node_s * prev = *head;

    pthread_mutex_lock(&lock_g);
    // Check for a NULL list
    if (curr == NULL)
    {
        rv = -1;  // NULL list
        goto rval_exit;
    }

    // Check head for match
    if (curr != NULL)
    {
        if (curr->fd == fd)
        {
            *head = curr->next;
            free(curr);
            goto rval_exit;
        }
    }

    // Check rest of list for a match
    while (curr->next != NULL)
    {
        // Save previous state and advance the current node
        prev = curr;
        curr = curr->next;

        // Check for a match
        if (curr->fd == fd)
        {
            // Eliminate dependency
            prev->next = curr->next;
            free(curr);
            goto rval_exit;
        }
    }

    rv = -2;  // fd not found

rval_exit:
	pthread_mutex_unlock(&lock_g);
	return rv;
}


//==============================================================================
// Description: Tears down and frees memory for the entire linked list.
// Input:  node_s ** head - Double pointer to head of list.
// Modify: Removes all clients from the list thereby destroying it.
// Return: None - void
//==============================================================================
static void destroyList(node_s ** head)
{
    node_s * curr = *head;
    node_s * next;

    // Traverse the list, deleting as we go!
    while (curr != NULL)
    {
        next = curr->next;
        free(curr);
        curr = next;
    }

    // Null-out head pointer
    *head = NULL;
}


// Make conditional compilation to prevent warnings and errors with -Wall and
// -Werror enabled.
#ifdef DEBUG_ON
//==============================================================================
// Description: Counts and returns the number of nodes in the linked list.
//              Useful for debugging.
// Input:  node_s * node - Pointer to head of list.
// Modify: None.
// Return: int - Number of nodes in the linked list.
//==============================================================================
static int getNumNodes(node_s * node)
{
    int count = 0;
    if (node == NULL)
    {
        return count;
    } else
    {
        while (node != NULL)
        {
            count++;
            node = node->next;
        }
        return count;
    }
}


//==============================================================================
// Description: Traverses and prints each node in the linked list.
//              Useful for debugging.
// Input:  node_s * node - Pointer to head of list.
// Modify: None.
// Return: None - void
//==============================================================================
// Print List
// Useful for debugging
static void printList(node_s * node)
{
    while (node != NULL)
    {
        printf("--- {Node} fd = %i ---\n", node->fd);
        node = node->next;
    }
}
#endif
